use std::{env, io};
use std::time::SystemTime;
use rand::{Rng, thread_rng};
use crossterm::execute;
use crossterm::cursor;
use crossterm::style::Print;

fn main() -> io::Result<()> {
    let args: Vec<String> = env::args().collect();
    let mut rng = thread_rng();
    let operation = match args[1].as_str() {
        "/" => |x: i32, y: i32| format!("{} + {}", x/y, x%y),
        "+" => |x: i32, y: i32| format!("{}", x+y),
        "-" => |x, y| format!("{}", x-y),
        "*" => |x, y| format!("{}", x*y),
        x => panic!("Unrecognized operation {}", x)
    };
    let er_digits: u16 = args[2].parse().unwrap();
    let and_digits: u16 = args[3].parse().unwrap();
    assert!(er_digits >= 1);
    assert!(and_digits >= 1);
    let thinger = rng.gen_range(10_i32.pow(er_digits as u32 - 1) .. 10_i32.pow(er_digits as u32));
    let thingand = rng.gen_range(10_i32.pow(and_digits as u32 - 1) .. 10_i32.pow(and_digits as u32));
    execute!(io::stdout(),
        Print(format!("\n{} {} {} = \n\n", thinger, args[1], thingand)),
        cursor::MoveUp(2),
        cursor::MoveRight(er_digits + and_digits + 6),
        cursor::SavePosition)?;
    let _ = io::stdin().read_line(&mut String::new());
    let start_time = SystemTime::now();
    let _ = io::stdin().read_line(&mut String::new());
    execute!(io::stdout(),
        cursor::RestorePosition,
        Print(format!("{}\n", operation(thinger, thingand))))?;
    let work_time = start_time.elapsed().unwrap().as_secs();
    if work_time > 60 {
        println!("{} m {} s", work_time / 60, work_time % 60);
    } else {
        println!("{} s", work_time);
    }

    Ok(())
}
