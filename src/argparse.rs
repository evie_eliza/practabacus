use std::fmt;
use std::fmt::write;

pub enum Flag {
    DigitRange { low: u16, high: u16 },
    Operation { name: String, op: fn(i32, i32) -> String},
    NoTrailingZeroes
}

impl fmt::Debug for Flag {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &self {
            Flag::DigitRange{low, high} => write!(f, "DigitRange {} {}", low, high),
            Flag::Operation{name, op: _} => write!(f, "Operation '{}'", name)
        }
    }
}

fn add(x: i32, y: i32) -> String { format!("{}", (x + y)) }
fn sub(x: i32, y: i32) -> String { format!("{}", (x - y)) }
fn div(x: i32, y: i32) -> String { format!("{} R {}", x / y, x % y) }
fn mul(x: i32, y: i32) -> String { format!("{}", (x * y)) }

pub fn parse_first(args: &[&str]) -> Option<(usize, Flag)> {
    match args {
        ["+", ..] => Some((1, Flag::Operation { name: String::from("+"), op: add})),
        ["-", ..] => Some((1, Flag::Operation { name: String::from("-"), op: sub})),
        ["/", ..] => Some((1, Flag::Operation { name: String::from("/"), op: div})),
        ["*", ..] => Some((1, Flag::Operation { name: String::from("*"), op: mul})),
        ["--no-trailing-zeroes", ..] | ["-Z", ..] => Some((1, Flag::NoTrailingZeroes)),
        ["-d", range_str, ..] => {
            if let Ok(digits) = range_str.parse() {
                Some((2, Flag::DigitRange { low: digits, high: digits}))
            } else if let Some((low_str, high_str)) = range_str.split_once('-') {
                if let (Ok(low), Ok(high)) = (low_str.parse(), high_str.parse()) {
                    Some((2, Flag::DigitRange{low, high}))
                } else {
                    panic!("unreadable digit range: {:?}", range_str)
                }
            } else {
                panic!("unreadable digit range: {:?}", range_str)
            }
        }
        _ => None
    }
}

pub fn parse_args(args: &[&str]) -> (usize, Vec<Flag>) {
    let mut index = 0;
    let mut flags = vec![];
    while let Some((consumed, flag)) = parse_first(&args[index..]) {
        index += consumed;
        flags.push(flag);
    }
    (index, flags)
}
